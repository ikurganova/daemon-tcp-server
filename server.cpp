
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <memory>
#include <iostream>
#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fstream> 

#define RUNNING_DIR	"/tmp"
#define LOCK_FILE	"exampled.lock"


typedef struct
{
	int sock;
	struct sockaddr address;
	unsigned int addr_len;
} connection_t;

class TCP_Server
{	

public:
	static int sock;
	static struct sockaddr_in address;
	static int port;	
	static int nFile;
	connection_t conn;

	static int init()
	{
		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock <= 0) 
		{ 
			std::cout << "Socket creation failed" << std::endl;
			return 1;
		}
		
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = INADDR_ANY;
		address.sin_port = htons(port);

		if (bind(sock, (struct sockaddr *)&address, sizeof(struct sockaddr_in)) < 0) 
		{ 
			std::cout << "command bind failed" << std::endl;
			return 2; 
		}
		if (listen(sock, 5) < 0)
		{ 
			std::cout << "command listen failed" << std::endl;
			return 3; 
		}
 
		int optval = 1;
		setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	}
	
	TCP_Server(){/*std::cout << "tcp ctr"<<std::endl;*/}
	
	static std::shared_ptr<TCP_Server> new_() {
        std::shared_ptr<TCP_Server> new_ = std::make_shared<TCP_Server>();
        return new_;
    }
    void proccess()
    {
		int fLen(0);
		struct timeval tv;
		tv.tv_sec = 60;
		tv.tv_usec = 0;
		setsockopt(conn.sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

		recv(conn.sock, &fLen, sizeof(int), MSG_NOSIGNAL);
		auto content = std::make_unique<char[]>(fLen);
		recv(conn.sock, content.get(), fLen, MSG_NOSIGNAL);

		nFile++;
		std::ofstream recFile;
		recFile.open ("rec_" + std::to_string(nFile));
		recFile << content.get();
		recFile.close();
    }


	~TCP_Server()
	{ 
		if (conn.sock >= 0)
		{
			shutdown(conn.sock, SHUT_RDWR);
			close(conn.sock); 
		}
	}
	static void handle()
	{
		while (true)
		{
			std::shared_ptr<TCP_Server> client = TCP_Server::new_();
			std::string s ;

			client->conn.sock = accept(TCP_Server::sock, &client->conn.address, &client->conn.addr_len);
			if (client->conn.sock <= 0)
			{
				//printf("fail");
			}
			else
			{
				client->proccess();
			}
		}
	}
};


void signal_handler(int sig)
{
	switch(sig) 
	{
		case SIGHUP:
		case SIGTERM:
			exit(0);
		break;
	}
}

void daemonize()
{
	int i,lfp;
	char str[10];
	if(getppid() == 1) return; /* already a daemon */
	i = fork();
	if (i < 0) exit(1); /* fork error */
	if (i > 0) exit(0); /* parent exits */
	/* child (daemon) continues */
	setsid(); /* obtain a new process group */
	for (i = getdtablesize(); i >= 0; --i) close(i); /* close all descriptors */
	i=open("/dev/null",O_RDWR); dup(i); dup(i); /* handle standart I/O */
	umask(027); /* set newly created file permissions */
	chdir(RUNNING_DIR); /* change running directory */
	lfp=open(LOCK_FILE,O_RDWR|O_CREAT,0640);
	if (lfp<0) exit(1); /* can not open */
	if (lockf(lfp,F_TLOCK,0)<0) exit(0); /* can not lock */
	/* first instance continues */
	sprintf(str,"%d\n",getpid());
	write(lfp,str,strlen(str)); /* record pid to lockfile */

	signal(SIGHUP,signal_handler); /* catch hangup signal */
	signal(SIGTERM,signal_handler); /* catch kill signal */
}

int TCP_Server::sock = -1;
int TCP_Server::port;
int TCP_Server::nFile = 0;

sockaddr_in TCP_Server::address;

int main(int argc, char ** argv)
{
	daemonize();

	if (argc != 2) exit(1);
	TCP_Server::port = atoi(argv[1]);

	if (TCP_Server::init()) exit(1);

	TCP_Server::handle();
	return 0;
}
