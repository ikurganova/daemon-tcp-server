
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <iostream> 
#include <string> 
#include <memory>     
#include <utility>
#include <netdb.h>
#include <fstream> 
  
  
class Client
{
protected:
    int sock; 
    struct sockaddr_in servaddr; 
public:
    enum ERROR
    {
        AMOUNT_OF_ARGUMENTS = 0,
        IP_ADDRESS,
        PORT,
        TYPE_PROTOCOL,
        SOCKET_CREATE,
        CONNECT,
        NO_ANSWER,
        FILE,
        AMOUNT_ERRORS
    };
    static std::array<std::string, Client::ERROR::AMOUNT_ERRORS> decryptionEr; 
    static void printEr(ERROR e)
    {
        std::cout << decryptionEr[e] << std::endl;
    }
    Client(std::string addr, int port)
    {
        memset(&servaddr, 0, sizeof(struct sockaddr_in));
        servaddr.sin_family = AF_INET; 

        if ((port > 0) && (port <= 0xffff)) servaddr.sin_port = htons(port); 
        else throw ERROR::PORT;

        servaddr.sin_addr.s_addr = inet_addr(addr.c_str()); 
        if (( in_addr_t)(-1) ==  servaddr.sin_addr.s_addr)  throw ERROR::IP_ADDRESS;

        sock = -1;
    }

    virtual void handleConnect() = 0;
    virtual ~Client(){};
    std::string nFile;
};

std::array<std::string, Client::ERROR::AMOUNT_ERRORS> Client::decryptionEr = 
{
    "Incorrect amount of arguments"
    ,"Incorrect IP address"
    ,"Incorrect port number"
    ,"Incorrect protocol type"
    ,"Socket creation failed"
    ,"Connect failed"
    ,"Server is not responding"
    ,"Can not open file"
} ;

class TcpClient : public Client 
{

public:
    TcpClient(std::string addr, int port) : Client(addr, port)
    {
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
            throw ERROR::SOCKET_CREATE;

        if (connect(sock, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) 
            throw ERROR::CONNECT;
    } 

    static std::shared_ptr<TcpClient> new_(std::string addr, int port) {
        std::shared_ptr<TcpClient> new_ = std::make_shared<TcpClient>(addr, port);//(new ex);
        return new_;
    }

    void handleConnect()override
    {
        struct timeval tv;
        tv.tv_sec = 5;
        tv.tv_usec = 0;
        setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);        

        std::ifstream is (nFile, std::ifstream::binary);
        if (is) 
        {
            int fLen(0);
            is.seekg (0, is.end);
            fLen = is.tellg();
            is.seekg (0, is.beg);

            auto content = std::make_unique<char[]>(fLen);
            is.read (content.get(),fLen);
            is.close();

            send(sock, &fLen, sizeof(int), MSG_NOSIGNAL);
            send(sock, content.get(), fLen, MSG_NOSIGNAL);
        }
        else throw ERROR::FILE;
    }

    ~TcpClient()
    {
        if (sock >= 0)
        {
            shutdown(sock, SHUT_RDWR);
            close(sock);
        } 
    }
};


int main(int argc, char ** argv) 
{ 
    try
    {
        if (argc != 4) throw Client::ERROR::AMOUNT_OF_ARGUMENTS;
         std::string addr(argv[1]);
         int port = atoi(argv[2]);
         
        auto cl = std::make_shared<TcpClient>(addr, port);
        cl->nFile = argv[3];
      
         cl->handleConnect();
    }
    catch(Client::ERROR er)
    {
        Client::printEr(er);
    }
    return 0;
} 